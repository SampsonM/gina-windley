export const projects = [
  {
    URL: "./fdp/1.jpg",
    isLocalImg: true,
    section: "projects",
    description: "project FDP description",
    alt: "Final Design Project",
    images: [
      {
        URL: "./fdp/1.jpg",
        alt: "Final design project",
        isLocalImg: true,
      }
    ]
  },
  {
    URL: "https://lh3.googleusercontent.com/g6ytPU--43XeMoQzp2TS_7yzxjiO9KvmGT4d1ZFVeWH0_30TOSp9NBtP0UZuCm2KYwOdwZlryYDPqf4XfJvWS3nEhOd85ROOYAjJXI_vIlTu4KDvJq8bJGQO98svgboS8vs9duGlkyqnPTn9HZQx35lzclZ_JE7aRbiiAWYl2QLCR-yeGl1fLPGwr0u-1G7CkjWiLsZju8rfoLGl2dF4yE8HbCfvhV7R7dwPFGcl5yPlIEXfm7Tz6NUH45sdIrm74eP0I0esGkvtJoA-Ne2uRXRk1_nH47GNtDiZcZ1sfV74ZHDZP4yqIHGllO_-cEjFo42Fm6HnYEEnyfAsEOQwJ8tCP1jN9VCPMDZQpIUCcK8N-HLVAQ9QAWeBY27PWAfCqsMj3j-nR0j6CjWT9trGvxiiQOnMr3M3ou-_qsakiTwmo4QtdqoKZh0sYOOfumMIZA6LGHOHbCoJ69Cfs2iF2utX_kzBdqmZ6ZDej_Mjjg_vdFVUhLcLFgWUZtSU2WgSqZx-MPNbkXbbHuOwUTJztNDqkJCrzG9ym8BgVXyBOTtbF4QbK5Mn-y-4EzoC65N1LP29sZm-QgZXSgvFjOb4xM6wFlrMJUmuJBwO3RI5ahIppJoIanuRq47umnQdZ2vYWw2mB-sooMhOL-qtQGlCA9erh2Dpkmg=w2504-h1878-no",
    section: "projects",
    description: "project DNB description",
    alt: "Design and build",
    images: [
      {
        URL: "https://lh3.googleusercontent.com/g6ytPU--43XeMoQzp2TS_7yzxjiO9KvmGT4d1ZFVeWH0_30TOSp9NBtP0UZuCm2KYwOdwZlryYDPqf4XfJvWS3nEhOd85ROOYAjJXI_vIlTu4KDvJq8bJGQO98svgboS8vs9duGlkyqnPTn9HZQx35lzclZ_JE7aRbiiAWYl2QLCR-yeGl1fLPGwr0u-1G7CkjWiLsZju8rfoLGl2dF4yE8HbCfvhV7R7dwPFGcl5yPlIEXfm7Tz6NUH45sdIrm74eP0I0esGkvtJoA-Ne2uRXRk1_nH47GNtDiZcZ1sfV74ZHDZP4yqIHGllO_-cEjFo42Fm6HnYEEnyfAsEOQwJ8tCP1jN9VCPMDZQpIUCcK8N-HLVAQ9QAWeBY27PWAfCqsMj3j-nR0j6CjWT9trGvxiiQOnMr3M3ou-_qsakiTwmo4QtdqoKZh0sYOOfumMIZA6LGHOHbCoJ69Cfs2iF2utX_kzBdqmZ6ZDej_Mjjg_vdFVUhLcLFgWUZtSU2WgSqZx-MPNbkXbbHuOwUTJztNDqkJCrzG9ym8BgVXyBOTtbF4QbK5Mn-y-4EzoC65N1LP29sZm-QgZXSgvFjOb4xM6wFlrMJUmuJBwO3RI5ahIppJoIanuRq47umnQdZ2vYWw2mB-sooMhOL-qtQGlCA9erh2Dpkmg=w2504-h1878-no",
        alt: "Design and build",
      },
      {
        URL: "https://lh3.googleusercontent.com/CNScxklsJlKJVsVqrn5NW5UKwTIh5yk7xobRxnb8k65aIgvMZzFjJaNu5anmRRXaPbrXnCrDKEppfoncT_NXuC8egeZbq-BVadINrQDkqUU2u9R3eI-6GARvXG9BOw7jsG-5-Tely3_R-TOEkbCtEBbhBZf9dJcY8ZnI-E4yM2exVDAgUTOfSjJ-uVmkREr5H8eMdNfsZF5EioQm436NGBU8tWSlYz5TICoVpZR2KYoQ237Be7cA-HWsig0K2y0bwipnDF475WQvRn26hVeQOpRY7xuHwCt8L8OxBF3BS9ZzDxwhpY7Tp_bVbLIbuTCfgQqziePOpSiriVpvd8kR77R04fhA59M9JJq5RJlI5edhysLRpG9doOCQK_SbWr0VBBb100VFHFEMneVrYS2dBNhsE4aJidBziVQyOZNUXiDP8yrWzpTmbK_JEIRCFhtNRuro09dBxTrvvdB_I2C6UYxRRfzjF2BLA6HULKl6bSnYTFQ3kAVc_fR2UEMorc4F2SkNrGfnA8G-boYXy8WRBoj51-f0TraNswnF4YUA1fM7EudKpeQ2hvdsbPKKmgzeHeUlQhE2HHmM6mWqYWAL5wW-7cU7NmMgNGsDoEa7BQGLhNsCkFHJPMTUQqHtXFNqbL5CRwHnUvHkOQ9nfm1uSo_Wb4glofo=w1600-h1200-no",
        alt: "Design and build"
      },
      {
        URL: "https://lh3.googleusercontent.com/01_-sxAS4mG70ZKc3H23yxPLZr29lznod2SrprLL-lEqabn8hjlmV0M9AQkVfipyYlDNoyj3FvdgKoLR5jCgm98cT4UHidPZ26C6uvnsVX0W1Leijvb2SX2RZSP_wFgyBvcGUmHBff_e6sExupNnOwUjQi6jUwQDZlqjFtoqiTzKvzxrS6oUC73u-0Yz8C7gfCOSn3qp0st_LAxTCRx77B45b83w9_XJt60pMOsAQtpwrPAYx_agfITEebGFcEpEsMU_jqglO0hMurhlGe_l5y-vnzauBOXurOsCo9QOtnIv31v0RrMg1H5NRxxFSQF5iS0u7ubsOpM76FfFg2w78eXmR9KXrcNIdsdB_7imaqU0lwBJ0FLk4KHH_LETtTlu0nZ7u5UTMVXydanv-s0WdbidDSILHth4invebABrXz1EkUbsCnQWuA5iNkk29PgQBooBrGkkQUX9WYMLU2kTRlirks4M7c-JJlLezEvA3d7C7CuOnnjmvD89qL9NrJP7D_noDfJEHpJdlHCbcd4vQOOvFHh2sRsYPFIPWpVk81aoaf5Yo5jlCgrkVt57FREm7okFLBU3K9rE35pW5tzFijO-4Bn6_T3fSHeo5T9nsW5JEqXpNoQRdtUDsMoFUlaNNZvZSwTjxtHLPMFDa241U0wragJHLyA=w2504-h1878-no",
        alt: "Design and build"
      },
      {
        URL: "https://lh3.googleusercontent.com/LnPcVBT-U8uJsUIh3cMaPFHsaXLp4EPoX7fpBKnmjjtE8BPn9iBKxxL7DmXZCa_xHnL94CIuVjAswg_2CsgXS_QlfUxVk8BhFWE_FpRC3znm8rFGrF3TQmPGA68HtXKXyVE7jFY9wFVQzVsOAJKuN7nanvn_RMtcn3sDoPoROqqHFYBcmtaCHZZd-poxFe04vkZx8OTz4oWHslv9t11QHqnIlGUvAavOhpsWK1azAx3ZW9Jrx4DHXBNSPLUdx1AJy8sMBytZGrCXRETLImvOZ_AgaOW6OQJzHnW46G0jklitvAzVes91-KXmVCJKDBd0EXQ8XXwBNi6ctMynfuElgBPrvmjoBhi15gcDaDEWcTkABLjB3ztQK_AhWI6eK2Wid93mbbFSaGC9wUwcGJYLDuop5wVHBxWB670guGY4w4A1YcMUfpz6BwhDlhNyDKgitGxoGl-s7pe03-svwA5eQ0dm8u4PrpitxQKAVt8ypBYIJaELpAcYEdk4KQ3mbNRQyLfvdUAEw6_oJ08cXIBc3HzGvWioY8NF1Lf9qLCdCoU2tJSMZ7wEUSMwFLgEYhWgEE_gvXSiCxPc35fT1Udtnj6jgNs3VN5Ja9qs5WUZkn-SAb58-jnfaMa_UPuRbAFKcjaswq3-yLXcQmmFuYD_ffeKKSjxciU=w2504-h1878-no",
        alt: "Design and build"
      },
      {
        URL: "https://lh3.googleusercontent.com/HpFcTkPhpEfDee4DmGYSGj9c7g3AgkUXxuKO47IdNZkaEBvyst73sIA959CEBkX1xIYD4Jdy0zLjSY5B40dknvxF_ycYv69MPCY9IsuEtLt6838vaLJdIJuhmJoPYCW9eDOQKEwCTg5HHccsgl0RJszSWklLLVLgSzcn81HTd4yoTqp6qvrAAiwWcxmCfpA3z5wA24YFy8A9ZAexGDiUeU3PCu9_RaAcz8PoWWjmLaRL0-33rvRoAmKCoigo4byn2-jSQEV8RFVntCXRRSjQkHo3Yx3ethI6HOuibw6bGItYlExW53AvUMvj7xGepBwgpWCxIB339JGSUP6XEKt8EJasEOjvFZQOJq8Z1QgECNcth3q56q1bq18YCAltUeiwLg2t8mXHms2YqZqVsuTWRQ4dUVg_0BR9tKhYyy2nkGOvnK9-cb1CDLA6LZ2-sVuH4ztKMpVVfWWr16Qvhm8W8fJDgM7HJRIT400y-Md_lAt6PCPidef7Yk3a3akcqUR8s0plL7xl80ywHZd6UHX81fupPojqml2uGRMRo6Y78wUdJ789kR0arMxuCwgIy6MXjxELGo93MRnK5gOs4-CngjSCT151KB2IVS3guMJcEwJSyCgrItud8rSJs-PRcZGbiko1LVfZ-4-9hAQYIJtzFlGv-CjrC18=w923-h447-no",
        alt: "Design and build"
      },
    ]
  },
  {
    URL: "https://lh3.googleusercontent.com/QS_adK5z569OU8SgYDqwpdw1EOtNXgUCiuUHHLt9ZXTXj1PHBdW9XG4vjFLihREDsoOjc1ouv19VOwEOa-hN1A4s9prqY8zNzNUaYzGmOqBwSErzw7DjDiau3JlU8cYwkTp384NU803BGRlPJigiSYGswA6mCXjuP-Xq_vWCGMHLLjJRVXrX7Rz2wjrMnbIHCUHKxxEBpUgA8NgDt9mCoKssj5rJoUeoNiqkJOOavBzcvulruMX1t_0mJbCXL6oazBiJnrWomZ7ii2B-gLrXLOyUiO5dLclBjX3VkUlB6hoVwtQWCNRbM4D48R4-ULkGAcy3Wo2SbLn5gO5w-jBILJ4b-I41900ykKiE7MPCiIXZ6Lc6NYmqXYCxbGJo8gDHSRv73YLbiQof4tD8HjsCRSNLOb3l-9CQBiHOfKGHkrmnOQYLhUWTWmsQmfjsh1XL2v5e6VYWCie8JC5rnvOE3G92Uam0HDr-vXJJWrVLLkRWOs3SsLf4sGHxp-U_WJYiZo9kOiCl9zbyCXqx_jebCzPmnpIbslIAFsS4joQ48w6sco0B7UFiNDCBfq34A7kCS6JWQfU2odEzVL-OxTvTUTkjVk-zD_ISkHikogV0doS0MtebgucRdyOPNxUB0zWuXxlqmh1ZTrGXRFGgVlRBP73kp96BgMA=w2362-h1073-no",
    section: "projects",
    description: "project north description",
    alt: "Northumberland Butward Bounds centre",
    images: [
      {
        URL: "https://lh3.googleusercontent.com/QS_adK5z569OU8SgYDqwpdw1EOtNXgUCiuUHHLt9ZXTXj1PHBdW9XG4vjFLihREDsoOjc1ouv19VOwEOa-hN1A4s9prqY8zNzNUaYzGmOqBwSErzw7DjDiau3JlU8cYwkTp384NU803BGRlPJigiSYGswA6mCXjuP-Xq_vWCGMHLLjJRVXrX7Rz2wjrMnbIHCUHKxxEBpUgA8NgDt9mCoKssj5rJoUeoNiqkJOOavBzcvulruMX1t_0mJbCXL6oazBiJnrWomZ7ii2B-gLrXLOyUiO5dLclBjX3VkUlB6hoVwtQWCNRbM4D48R4-ULkGAcy3Wo2SbLn5gO5w-jBILJ4b-I41900ykKiE7MPCiIXZ6Lc6NYmqXYCxbGJo8gDHSRv73YLbiQof4tD8HjsCRSNLOb3l-9CQBiHOfKGHkrmnOQYLhUWTWmsQmfjsh1XL2v5e6VYWCie8JC5rnvOE3G92Uam0HDr-vXJJWrVLLkRWOs3SsLf4sGHxp-U_WJYiZo9kOiCl9zbyCXqx_jebCzPmnpIbslIAFsS4joQ48w6sco0B7UFiNDCBfq34A7kCS6JWQfU2odEzVL-OxTvTUTkjVk-zD_ISkHikogV0doS0MtebgucRdyOPNxUB0zWuXxlqmh1ZTrGXRFGgVlRBP73kp96BgMA=w2362-h1073-no",
        alt: "Northumberland outward bound"
      },
      {
        URL: "https://lh3.googleusercontent.com/6rWccviTGvAf6_Mp4KBsD8nMIwlcm043S34NLFSmtagAh3j0wdm0modaciSA2HfVQ1sqIkTtzriP8BJ8W6y8W1AYAIFdSv-nt9cFLgVLjTSz3lK9aw3aMm17e409BxkjK2hsXDxe6B3whdmdmC9XBoMdL8i9GZcPquP49dBiJ7Y_wC_rnwPlwaEfCex0nRYn3EpL3fXWDnuUnm4b68bpiuDBnSQlac0nqZONoUougSWV7QcihbGMrKUh4Bpv7bbuCwDXCIWe69wASTBqz0B8rUtJwoIxLJ7ZUZgW_BHisPqYFv5ChDmAaGTIBA3o3bJknCsJb8gapS8O1sxRuviZpXtfdkMej2Z6BY65nO0cXNudq6oD9ZjJt328TD3i72qV3r6isJX7hHirJzSxjV6P1p2JM9MNARbDLaZZsr15jj2qGXsixBgsjPh5ZiwjyATUZZCvsvyBVed3c0somyBnyukrFZr4qkWeOheUIxS9-tTrhUnCEqyNJMk1byv4VrclcBUA1r7PkWL00no7KrhrQ2PMb3Nmt_J96fafFEjmxpAF10F36g9AxVBee3_RrRYV7lZcAPG6ea-Jv_0rSGrf96s0hXxRnpnsIWah8h_rj5BETqICsZUHay0kWXvxwaZnznAI2YQ_r7Jc1VKNLBgEcQ5LCm3RzGQ=w2362-h1659-no",
        alt: "Northumberland outward bound"
      },
      {
        URL: "https://lh3.googleusercontent.com/sXKeHQm1LwkAjsI1GWp6ahQ31gpnMOhqedUgX7X-vX7404qQ9q8JgTJXpaR3CsPcf2HhoGP8Qfcw86ChUrRhdRH_ZcU7T_Lxcy6axliw2MmWYw-XljHax5RF4Bx-7m0iOtd8LJqwZUwH67M1JZSFrFKGs2dT6VlCP5FQLnzDttHtMMnmxZrrx8J3FefUGdqGfYHg6TrCP1KOBFFlTd0Ma8WSDnEqOJrLwp0N8EX5GgtSg5prUnTrUq1wHY3g0vgj43mO9252rui7N2g-ZMS2qkINX_iBh7ZduY3U3mF8cXKA7mYN2a4Qyq2naprbwnrff_kKlwNvc3EnuW6bZlMvYAaYzuEkyYnqH4-YjT-OtkGLqpW0cT_MVYvI8s0wliAOqTUPg6o5Xbf4gSrP5Z-TeMeUMe1rfDdbB16V9IqthM9MiEZHxuPm9I1eefLyeC5xwe2PXRPUKXiBAUGmb-d6dVe6luBTmFl7VWu9beykREroi1E76I5Ox8CFhkdPn6PuE82-9Pgb3iyU1JU2UB1SlhOpHUBEDKF0YuMCUyGHAqEKrCYShbUbFJnAPIsCejjeMa5DRViqRQrVQmTmFABDzVGgix4u1UFBX_y7aqlEMDlS3oiHIJWk59-IpaX5u8e7vmVTGAWfmCF5UXrZ10nPLxvTTzOIUvQ=w2362-h1609-no",
        alt: "Northumberland outward bound"
      },
      {
        URL: "https://lh3.googleusercontent.com/_ycpLcLvD2vq9ftZJLVnSQ1Bs1eg7LmkvqYZfO84j1FvT2bvRHXvfVq0Mk3Cl9WX11amZ0rnRc2LmeX1VmRUz60Dn22AWwth8LDZuxApoI4yZJeTkdntyiB0hVr1jjHpAa-X6rGqzKGlChljbKeOuyTogATTSPtH4j1zNBA_bWboo_H02x9oQH4_bmGAMi0LiA83CIPIxEIvrsMRj5g3i0vcqBs-oAZ0fpgISPoiFj_rDI7DNQez3S5yLmh230H3SrMRlQHotiJa5oOGQ_PNDH_hZPyvu6OEkq-oap5aLAteKfz8vhMQCZmP9Q48F506ojM6nsGUgb-scFWtm7aNuwhEyWny8rYO2lmY0-92yQ7Pyd-aNpmol1D9EqQBcA3dQsAAwId7KykRZUvN-6JxkY3NtqHTd0ikv-zgYPbHW-qkE5gZhSxZFggQMcEt7yHElmW5q_ywm7BVt_VaFCY9tSopUB6ZaXnG42zSTwCqdAQor5Ft11plrkfqnLDDaiPlnLXhXtV7zPvT_G9hgLWH3ASgaD0YiFsyzK9_VY09VnfiGJopx1qEkasXShp_cNCaFDOeaYpMNJ7IjIP4am-1HpmELVljgAagnggPv9GIeEC3lg4pJ3IwO9c-_7RKl2xeCaaf0wMwbP2h9jH6YhjfJXeL49yW8b8=w2754-h1878-no",
        alt: "Northumberland outward bound"
      }
    ]
  }
]

export const volunteering = [
  {
    URL: './building/1.jpg',
    section: 'volunteering',
    isLocalImg: true,
    alt: 'Volunteered building',
    description: 'volunteer project 3 description',
    images: [
      {
        URL: "./building/1.jpg",
        alt: "volunteer project 3",
        isLocalImg: true,
      },
    ]
  },
  {
    URL: 'https://lh3.googleusercontent.com/LlfdIF2yz8oNsz8t3nLgwBzvYRGlEZe20fKNEopKa6MttUXbLCXx73HhMBf-qeb14aONN-ojeLg9JtCSo9ZjMXmmSSLj-xlgUAtWMKaqZs-m6uMH1Jiifm4dde989BZJ4K42GeJv3byU4yj6u7Mj_EGj3OMfvg0D096CxubxGoUSS7frYNoZRZDY9-XM2y2C82KCOA2r3nCtBxHwf3Ctf_8MGjCU67lpvw4U_hi-wRJxOtI2659uVXP9MhM5_E49IHRg1IDMqAvfUcjIbr5loAB_gd_pbsUl5Pku1q3YAgHy9gvgQCxa4OSMvG4OlevRcqbAGsiI-4mF2yES3n7rxjPrS8pCgclDsWONqfO6ghrpQm_V3kXLJOCfClBPDv3pkETb-FguWs9ZmFgq0klQrJyHq57aNUSdQjYyYslkFdK-JIe8B-JIB6reWI4Hrcp0kiqIbmovv3jMc6WnL9SGN-kkPA-fCgCoMMhDHi2bCFLAdyCjtAyVxteUG93RDPKSRLI77qbNUslA4ksPb6TLzOFRqJYZmKz9AHhtFoHgbJV0Ohf0zlPMaZiCWcjCNbxRoNuGbbp6ZT7glRouP4JxHqn1xcfB8ET7DDbSRMf7gOYN-FYLBav31BVBxZz5OzzfpOMlsbBBR6i87Ps7MRcfHJpML9ZLGPM=w3072-h1750-no',
    section: 'volunteering',
    alt: 'Ghana rammed earth',
    description: 'volunteer project 1 description',
    images: [
      {
        URL: "https://lh3.googleusercontent.com/LlfdIF2yz8oNsz8t3nLgwBzvYRGlEZe20fKNEopKa6MttUXbLCXx73HhMBf-qeb14aONN-ojeLg9JtCSo9ZjMXmmSSLj-xlgUAtWMKaqZs-m6uMH1Jiifm4dde989BZJ4K42GeJv3byU4yj6u7Mj_EGj3OMfvg0D096CxubxGoUSS7frYNoZRZDY9-XM2y2C82KCOA2r3nCtBxHwf3Ctf_8MGjCU67lpvw4U_hi-wRJxOtI2659uVXP9MhM5_E49IHRg1IDMqAvfUcjIbr5loAB_gd_pbsUl5Pku1q3YAgHy9gvgQCxa4OSMvG4OlevRcqbAGsiI-4mF2yES3n7rxjPrS8pCgclDsWONqfO6ghrpQm_V3kXLJOCfClBPDv3pkETb-FguWs9ZmFgq0klQrJyHq57aNUSdQjYyYslkFdK-JIe8B-JIB6reWI4Hrcp0kiqIbmovv3jMc6WnL9SGN-kkPA-fCgCoMMhDHi2bCFLAdyCjtAyVxteUG93RDPKSRLI77qbNUslA4ksPb6TLzOFRqJYZmKz9AHhtFoHgbJV0Ohf0zlPMaZiCWcjCNbxRoNuGbbp6ZT7glRouP4JxHqn1xcfB8ET7DDbSRMf7gOYN-FYLBav31BVBxZz5OzzfpOMlsbBBR6i87Ps7MRcfHJpML9ZLGPM=w3072-h1750-no",
        alt: "volunteer project 3"
      },
      {
        URL: "https://lh3.googleusercontent.com/M7nfmV3WsPEA1iCb8d7MBl4VGVcRxiJRpssuYgBRq0DqMUzJ-XYgkCsV3EdObq6CPDZR5D9m5y4t4kzr_I3-aPvlSRH1b9QvMkeSIT4FY-_m-Ed18_5lBvGYnOaTSjGV_z0_YOWKCqTS4vG_hBSHcWEssy8wRWAyyxKRrxLDsaInG5TjcA0ep28BJ5Gv5qZEA5OJ2sTqZq9poh88N7WMx4caJxfbVNRwNKOSpCBUofcYW6ec_eTGIMEr-uk_Gpd1qrye7zamw6UmUrVDAnjwtLN8brw6ALfvTBvVd_-w0YJasN1siP2MYFEygqu_zpsI1403tlwplEzq0c1tk7QtTntGcjcVwUaKc70-rg9UdH-JtnuOdIcJA_FVo_cTYbgxkiJeRXm9p1GrrNA9pmkmSuZcCRFdNrb_qbJt5dPeqak4JIfJyUYjOnUhFk-Y_GcKBYF9pBTeTzmKedG6CWXS-AjGmwzjrCZyNhY82Xd0mAydqym7XmJfvdTRbhpifinQ7j9Pi8N_B_k9Arvme-_CswIIJ0ympPo6pOzWrp5R7Demu1-1FHkWe-tOajF_vm3Q-I7_L7DcPx1HubDlO_MPvdZKxXDjQ9hdMwlKwx5h10wrXgtYSwyW1c2ybbE4NVEdtGI9dRJPwtiLe58i_USAcUP2gYo7yZ4=w1080-h608-no",
        alt: "volunteer project 3"
      },
      {
        URL: "https://lh3.googleusercontent.com/IgH-LLmgo506x1QeZS7vCg0OqRKFj0PED-H9rhsZrsEbU9J_Z8k-qF1teM5zX4ElnaAjhk98RojX6aQ0Vy-TJbGeZfq6tFFKwqcgU71gwr8JP6lrVL-5FMctTDri3Phh6Bv8_tAf3eawnrFif1Xar_K6VTiSWMS0dCAZ1ogJ_sqmgzJXqjqLEQTvpK04rdPrN0JzSYVFG8uYwBYLjTd1Q00XAO8lkltVVUpxV2Ns7HtZm8MDVh5OZbud42ArTWLrHh6UKku9RbIJvPzP8rotAAGMl_vuRCx2_7PaJ8LQL5gAR16eo_-7hgXAGdoOguGm6t8msumhOFbvr-JJ134D1Cmyew4vjlCUE-rvFo1wGNwwd_TwgZicpiwIaugWpFDCZJcnIB7FNeEqSOqPDFwsHZ_KmoBNOGyBP1Ifbva2jttU22d2heLjqPQzekLPC3sE374QRag_80zYrK9UqjqMFXrP1zhn0SCmXyQEFYtKXFe3VVX2KGw-07AXOkuwdTSOIU69Xdwxw0EFAENFGiEWsOrcCHi6zRbn0-e17loTrB_mbVrMBvurEbE9DPqMSVRZOgJ2TTRIaVdTpGJH0yZs0xamj8uAsMTDaHHySnUnTs_nV_c-aU2_ddbG2t-bEY4pz1NQsg5RFz3LxwQT1JKNKC4qetRCvPU=w805-h532-no",
        alt: "volunteer project 3"
      },
    ]
  }
]

export const about = {
  URL: "./images/gina.png",
  description: "About me",
  alt: "Gina Windey"
}
